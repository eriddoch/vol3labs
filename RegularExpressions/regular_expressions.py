# regular_expressions.py
"""
Volume 3: Regular Expressions.
Eric Riddoch
Tuesday, September 3, 2019
"""

import re

# Problem 1
def prob1():
    """Compile and return a regular expression pattern object with the
    pattern string "python".

    Returns:
        (_sre.SRE_Pattern): a compiled regular expression pattern object.
    """
    
    # define pattern
    return re.compile('python')

# Problem 2
def prob2():
    """Compile and return a regular expression pattern object that matches
    the string "^{@}(?)[%]{.}(*)[_]{&}$".

    Returns:
        (_sre.SRE_Pattern): a compiled regular expression pattern object.
    """
    
    # define pattern
    return re.compile(r"\^\{@\}\(\?\)\[\%\]\{\.\}\(\*\)\[_\]\{\&\}\$")

# Problem 3
def prob3():
    """Compile and return a regular expression pattern object that matches
    the following strings (and no other strings).

        Book store          Mattress store          Grocery store
        Book supplier       Mattress supplier       Grocery supplier

    Returns:
        (_sre.SRE_Pattern): a compiled regular expression pattern object.
    """
    
    # define pattern
    return re.compile(r'^(Book|Mattress|Grocery) (store|supplier)$')

# Problem 4
def prob4():
    """Compile and return a regular expression pattern object that matches
    any valid Python identifier.

    Returns:
        (_sre.SRE_Pattern): a compiled regular expression pattern object.
    """

    # do the needful
    return re.compile(r'[A-z_]\w*\s*(=\s*([A-z]\w*|\'[^\']*\'|\"[^\"]*\"|[0-9]*\.?[0-9]*))?')

# Problem 5
def prob5(code):
    """Use regular expressions to place colons in the appropriate spots of the
    input string, representing Python code. You may assume that every possible
    colon is missing in the input string.

    Parameters:
        code (str): a string of Python code without any colons.

    Returns:
        (str): code, but with the colons inserted in the right places.
    """

    # define pattern    
    pattern = re.compile(r'(.*(if|elif|else|for|while|try|except|finally|with|def|class).*)')
    return pattern.sub(r'\1:', code)

# Problem 6
def prob6(filename="fake_contacts.txt"):
    """Use regular expressions to parse the data in the given file and format
    it uniformly, writing birthdays as mm/dd/yyyy and phone numbers as
    (xxx)xxx-xxxx. Construct a dictionary where the key is the name of an
    individual and the value is another dictionary containing their
    information. Each of these inner dictionaries should have the keys
    "birthday", "email", and "phone". In the case of missing data, map the key
    to None.

    Returns:
        (dict): a dictionary mapping names to a dictionary of personal info.
    """

    def extract(pattern, string):
        """
        Extract data from string and return old string with the match removed.
        Assume there will be only 0 or 1 matches.
        """
        matches = re.findall(pattern, string)
        if len(matches) == 0:
            match = None
        else:
            match = matches[0]
        truncated_string = re.sub(pattern, '', string)

        return match, truncated_string

    def normalize_date(date):
        """
        Return date in the form mm/dd/yyyy
        """
    
        if date is None:
            return None
        
        parts = date.split('/')
        mm = parts[0]
        if len(mm) < 2:
            mm = '0' + mm
            
        dd = parts[1]
        if len(dd) < 2:
            dd = '0' + dd
            
        yyyy = parts[2]
        if len(yyyy) < 4:
            yyyy = '20' + yyyy
            
        return '/'.join([mm, dd, yyyy])

    def normalize_phone(phone):
        """
            1-800-999-9999 -> (800)999-9999
            999-999-9999   -> (999)999-9999
            (800)999-9999  -> (800)999-9999
        """
        
        if phone is None:
            return None
        
        # 1. remove '1-' and '1(' from beginning
        phone = phone.lstrip('1-').lstrip('1(')
        
        # 2. remove all formatting
        phone = re.sub(r'[^0-9]', '', phone)
        
        # 3. get all parts
        area = phone[:3]
        pre  = phone[3:6]
        post = phone[6:]
        
        # 4. format correctly
        return f"({area}){pre}-{post}"

    # search patterns
    email_pattern = r'[\S]+@[\S]+'
    date_pattern =  r'[0-9/]{5,}'
    phone_number_pattern = r'[\(\)\-0-9]{6,}'

    # result
    data_dict = dict()

    # parse file
    lines = []
    with open('fake_contacts.txt', 'r') as file:
        lines = file.readlines()
        for line in lines:
            
            name = email = date = phone = None

            # extract email
            email, line = extract(email_pattern, line)

            # extract phone number
            phone, line = extract(phone_number_pattern, line)
            phone = normalize_phone(phone)

            # extract date
            date, line = extract(date_pattern, line)
            date = normalize_date(date)

            # extract name
            name = line.strip()

            # add to data dict
            data_dict[name] = {
                'birthday': date,
                'email':    email,
                'phone':    phone
            }
    
    return data_dict






