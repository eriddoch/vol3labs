import regular_expressions as reg

def test_prob1():
    
    string = "This sentence has the word python in it."
    assert bool(reg.prob1().search(string)) == True

def test_prob2():
    
    string = "Here is this silly string: ^{@}(?)[%]{.}(*)[_]{&}$"
    assert bool(reg.prob2().search(string)) == True

def test_prob3():

    test_strings_1 = 'Book store|Mattress store|Grocery store|Book supplier|Mattress supplier|Grocery supplier'.split('|')
    test_strings_2 = [
        'dummy string', 
        'dummy store',
        'Book store supplier']

    answers_1 = [True] * len(test_strings_1)
    answers_2 = [False] * len(test_strings_2)

    for index, string in enumerate(test_strings_1):
        print(bool(reg.prob3().match(string)), string)
        assert bool(reg.prob3().match(string)) == answers_1[index]
        assert bool(reg.prob3().search(string)) == answers_1[index]

    for index, string in enumerate(test_strings_2):
        assert bool(reg.prob3().match(string)) == answers_2[index]
        assert bool(reg.prob3().search(string)) == answers_2[index]

def test_prob4():
    
    matches = ["Mouse", "compile", "_123456789", "__x__", "while", "max=4.2", "string= ''", "num_guesses"]
    nonmatches = ["3rats", "err*r", "sq(x)", "sleep()", " x", "300", "is_4=(value==4)", "pattern = r'^one|two fish$'"]

    pattern = reg.prob4()

    for match in matches:
        assert bool(pattern.fullmatch(match))

    for non in nonmatches:
        print(non, pattern.fullmatch(non), bool(pattern.fullmatch(non)))
        assert not bool(pattern.fullmatch(non))

def test_prob5():

    test_input = """
    k, i, p = 999, 1, 0
    while k > i
        i *= 2
        p += 1
        if k != 999
            print("k should not have changed")
        else
            pass
    print(p)
    """

    test_output = """
    k, i, p = 999, 1, 0
    while k > i:
        i *= 2
        p += 1
        if k != 999:
            print("k should not have changed")
        else:
            pass
    print(p)
    """

    output = reg.prob5(test_input)
    print(output)
    assert output == test_output