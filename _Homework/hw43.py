from scipy.stats import norm
import numpy as np

""" Problem  """

# get std normal
std_norm = norm(loc=0, scale=1)

# take 300 samples
n = 300
x = np.sort(std_norm.rvs(n))

# estimated c.d.f. from empirical distribution
F_hat = lambda y: (y > x).mean()

# tools to determine 90% confidence interval for the true normal cdf
calc_e_n = lambda alpha, n: np.sqrt( (1/(2*n)) * np.log( 2 / alpha ) )
e_n = calc_e_n(0.1, n)
L = np.vectorize(lambda y: np.max([F_hat(y) - e_n, 0]))
U = np.vectorize(lambda y: np.min([F_hat(y) + e_n, 1]))

# check how often the cdf is within this confidence interval
ts = np.linspace(-20, 20, 1000)
F = std_norm.cdf(ts)
within = (L(ts) <= F) & (F <= U(ts))
print('(Normal) the proportion of y in [-20, 20] for which the conf. int. holds is', within.mean())

from scipy.stats import cauchy

x = cauchy.rvs(size=n)

# estimated c.d.f. from empirical distribution
F_hat = lambda y: (y > x).mean()

# tools to determine 90% confidence interval for the true normal cdf
calc_e_n = lambda alpha, n: np.sqrt( (1/(2*n)) * np.log( 2 / alpha ) )
e_n = calc_e_n(0.1, n)
L = np.vectorize(lambda y: np.max([F_hat(y) - e_n, 0]))
U = np.vectorize(lambda y: np.min([F_hat(y) + e_n, 1]))

# check how often the cdf is within this confidence interval
ts = np.linspace(-20, 20, 100)
F = cauchy.cdf(ts)
within = (L(ts) <= F) & (F <= U(ts))
print('(Cauchy) the proportion of y in [-20, 20] for which the conf. int. holds is', within.mean())