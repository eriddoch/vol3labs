#!/bin/bash
# download openMPI and extract it
wget https://download.open-mpi.org/release/open-mpi/v4.0/openmpi-4.0.2.tar.gz
tar -zxf openmpi-4.0.2.tar.gz
cd openmpi-4.0.2
# configure the files
./configure --prefix=/usr/local/openmpi
# compile openMPI
make all
# install openMPI
sudo make install
# add the variable to the path
export PATH=$PATH:/usr/local/openmpi/bin
# install mpi4py
pip3 install mpi4py --user
