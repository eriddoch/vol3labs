from mpi4py import MPI
from sys import argv
from scipy import linalg as la
import numpy as np

# Problem 4
"""The n-dimensional open unit ball is the set U_n = {x in R^n : ||x|| < 1}.
Estimate the volume of U_n by making N draws on each available process except
for the root process. Have the root process print the volume estimate.

Command line arguments:
    n (int): the dimension of the unit ball.
    N (int): the number of random draws to make on each process but the root.

Usage:
    # Estimate the volume of U_2 (the unit circle) with 2000 draws per process.
    $ mpiexec -n 4 python problem4.py 2 2000
    Volume of 2-D unit ball: 3.13266666667      # Results will vary slightly.
"""

# establish comm
COMM = MPI.COMM_WORLD
RANK = COMM.Get_rank()

# get arguments
n = int(argv[1])
N = int(argv[2])

# get number of nodes
num_nodes = COMM.Get_size()

# Non-root nodes
if RANK > 0:

    # draw N vectors from [-1, 1]^n
    samples = np.random.uniform(-1, 1, (n, N))

    # calculate the norm of each of the N vectors
    U_n = la.norm(samples, axis=0) < 1

    # proportion
    proportion = U_n.mean()

    # send the proportion to the 0 rank
    COMM.Send(proportion, dest=0)

# Root node averages results of the workers compute the MC approx of the volume
if RANK == 0:

    # collect all the proportions from each of the worker nodes
    final_proportion = np.zeros(1)
    for rank in range(1, num_nodes):
        proportion = np.zeros(1)
        COMM.Recv(proportion, source=rank)
        print(proportion)
        final_proportion += proportion

    # calculate the final proportion
    final_proportion /= (num_nodes - 1)

    # calculate the area
    box_area = 2**n
    unit_ball_area = final_proportion * box_area

    print(f'Area of {n}-dim unit ball = {unit_ball_area}')
