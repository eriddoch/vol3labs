from mpi4py import MPI
from sys import argv
import numpy as np

# Problem 2
"""Pass a random NumPy array of shape (n,) from the root process to process 1,
where n is a command-line argument. Print the array and process number from
each process.

Usage:
    # This script must be run with 2 processes.
    $ mpiexec -n 2 python problem2.py 4
    Process 1: Before checking mailbox: vec=[ 0.  0.  0.  0.]
    Process 0: Sent: vec=[ 0.03162613  0.38340242  0.27480538  0.56390755]
    Process 1: Recieved: vec=[ 0.03162613  0.38340242  0.27480538  0.56390755]
"""

# establish comm
COMM = MPI.COMM_WORLD
RANK = COMM.Get_rank()

# get argument
n = int(argv[1])

if RANK == 1:

    # send numpy of size n to process 0
    num_buffer = np.random.randn(n)
    COMM.Send(num_buffer, dest=0)
    print(f'Process 1 sent: {num_buffer}')

elif RANK == 0:

    # recieve the array in process 0
    num_buffer = np.zeros(n)
    COMM.Recv(num_buffer, source=1)
    print(f'Process 0 recieved: {num_buffer}')
