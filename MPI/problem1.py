from mpi4py import MPI

# Problem 1
"""Print 'Hello from process n' from processes with an even rank and
print 'Goodbye from process n' from processes with an odd rank (where
n is the rank).

Usage:
    $ mpiexec -n 4 python problem1.py
    Goodbye from process 3                  # Order of outputs will vary.
    Hello from process 0
    Goodbye from process 1
    Hello from process 2

    # python problem1.py
    Hello from process 0
"""

# open up connection
COMM = MPI.COMM_WORLD
RANK = COMM.Get_rank()

# even ranked connections say hello
if RANK % 2 == 0:
    print(f"Hello from process {RANK}")
else:
    print(f"Goodbye from process {RANK}")




artist_name = str(track['artists'][0]['name'])
track_name  = str(track['name'])
track_id    = str(track['id'])
popularity  = str(track['popularity'])