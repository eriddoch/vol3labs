from mpi4py import MPI
import numpy as np

# Problem 3
"""In each process, generate a random number, then send that number to the
process with the next highest rank (the last process should send to the root).
Print what each process starts with and what each process receives.

Usage:
    $ mpiexec -n 2 python problem3.py
    Process 1 started with [ 0.79711384]        # Values and order will vary.
    Process 1 received [ 0.54029085]
    Process 0 started with [ 0.54029085]
    Process 0 received [ 0.79711384]

    $ mpiexec -n 3 python problem3.py
    Process 2 started with [ 0.99893055]
    Process 0 started with [ 0.6304739]
    Process 1 started with [ 0.28834079]
    Process 1 received [ 0.6304739]
    Process 2 received [ 0.28834079]
    Process 0 received [ 0.99893055]
"""

# establish comm
COMM = MPI.COMM_WORLD
RANK = COMM.Get_rank()


# generate random number
num = np.random.randn(1)
# have highest rank send num to the first
dest_rank = 0 if RANK == COMM.Get_size() - 1 else RANK + 1
# send it to the next process
COMM.Send(num, dest=dest_rank)
print(f'Process {RANK} sent {num}')


# recieve a number
receiving = np.zeros(1)
source_rank = RANK - 1 if RANK != 0 else COMM.Get_size() - 1
COMM.Recv(receiving, source=source_rank)
print(f'Process {RANK} received {receiving}')


'''AT LEAST SOMEONE NEEDS TO SEND BEFORE THEY RECEIVE OR ELSE
ALL PROCCESSES WILL WAIT INDEFINITELY'''
