# -*- coding: utf-8 -*-
"""pandas2.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/1WOTmUIiv6gmH6tDwIc07fEH3UWzVH1vy

# Pandas 2

## Name

## Class

## Date
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


"""# Problem 1"""

def prob1():
    """
    Create 3 visualizations of the crime data set.
    One of the visualizations should be a histogram.
    The visualizations should be clearly labelled and easy to understand.
    """
    
    # get data
    df = pd.read_csv('crime_data.csv')
    
    # make plots
    ax1 = df.plot.scatter('Violent', 'Murder', title='Violent Crimes vs. Murder')
    df.plot.scatter('Population', 'Murder', title='Population vs. Murder')
    df.plot.scatter('Population', 'Total', title='Population vs. Total Crime')
    plt.show()
    
    return df



"""# Problem 2"""

def prob2():
    """
    Use visualizations to identify if trends exists between Forcible Rape
    and
        1. Violent
        2. Burglary
        3. Aggravated Assault
    Plot each visualization.
    Return a tuple of booleans. Each entry should identify whether that 
    element correlates or not.
    """
    
    # get data
    df = pd.read_csv('crime_data.csv')

    # plot
    ax = pd.plotting.scatter_matrix(df[['Forcible Rape', 'Violent', 'Burglary', 'Aggravated Assault']])
    
    plt.show()
    
    return True, False, True
    

"""# Problem 3"""

def prob3():
    """
    Use crime_data.csv to display the following distributions.
        1. The distributions of Burglary, Violent, and Vehicle Theft across all crimes
        2. The distributions of Vehicle Thefts by the number of Robberies
    """
    
    # get data
    df = pd.read_csv('crime_data.csv', index_col=0)

    # plot scatter matrix
    ax = pd.plotting.scatter_matrix(df[['Forcible Rape', 'Violent', 'Burglary', 'Aggravated Assault']])
    plt.show()
    
    # question 1
    df.plot.box(y=["Burglary", "Violent", "Vehicle Theft"])
    plt.title("Crime Distributions")
    
    # question 2
    df.plot(kind="Hexbin", x="Vehicle Theft", y="Robbery", gridsize=20)
    plt.title("Vehicle Theft vs Robbery")
    
    plt.show()


"""# Problem 4"""

def prob4():
    """
    Answer the following questions with the College dataset
    
    1. Hexbin between top10perc and PhD
    2. Looking at applications, acceptance, and enrollment
    3. Looking at s.f.ratio and graduation rate
    4. Private and s.f.ratio
    5. Out of state and room and board
    6. Compare schools
    """
    
    df = pd.read_csv('college.csv', index_col=0)
        
    # 1.
    ax = df.plot.scatter('Top10perc', 'PhD', title='Percent PhDs vs Percent Top 10 in HS')
    ax.set_xlabel('% Student Body in Top 10 Percent of HS Graduating Class')
    ax.set_ylabel('% Student Body in PhD Program')
    
    # 2.
    df.plot.box(y=["PhD", "Terminal", "Top10perc"])
    plt.title('PhD\'s vs. Graduates vs. HS Grads')
    
    # 3.
    df.plot(kind="Hexbin", x="Outstate", y ="Grad.Rate", gridsize=20)
    plt.title("Frequencies of Outstate Graduates")
    
    plt.show()
    
    return df
