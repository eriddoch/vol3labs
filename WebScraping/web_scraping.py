"""Volume 3: Web Scraping.
Eric Riddoch
"""

import requests
from bs4 import BeautifulSoup
import re
import pandas as pd
import matplotlib.pyplot as plt

# Problem 1
def prob1():

    # get the html payload
    res = requests.get('http://www.example.com')
    html = res.text

    try:
        with open('example.html', 'w') as file:
            file.write(html)
    except Exception:
        print('File not found!')
        return
        
    # unnecessary return
    return html
    
    
# Problem 2
def prob2():
    """Examine the source code of http://www.example.com. Determine the names
    of the tags in the code and the value of the 'type' attribute associated
    with the 'style' tag.

    Returns:
        (set): A set of strings, each of which is the name of a tag.
        (str): The value of the 'type' attribute in the 'style' tag.
    """
    
    # parse html
    sopa = None
    with open('example.html', 'r') as file:
        sopa = BeautifulSoup(file)
        
    # get tag names
    tag_names = { tag.name for tag in sopa.find_all() }
    
    # get style text
    style_text = sopa.find_all('style').attrs['type']
        
    return tag_names, style_text


# Problem 3
def prob3(code):
    """Return a list of the names of the tags in the given HTML code."""
        
    soup = BeautifulSoup(code)        

    # get tag names
    tag_names = [ tag.name for tag in soup.find_all() ]
        
    return tag_names


# Problem 4
def prob4(filename="example.html"):
    """Read the specified file and load it into BeautifulSoup. Find the only
    <a> tag with a hyperlink and return its text.
    """
    
    # parse html
    sopa = None
    with open(filename, 'r') as file:
        sopa = BeautifulSoup(file)
        
    # find anchor tags
    anchor = sopa.find('a', {'href': True})
    
    return anchor.text


# Problem 5
def prob5(filename="san_diego_weather.html"):
    """Read the specified file and load it into BeautifulSoup. Return a list
    of the following tags:

    1. The tag containing the date 'Thursday, January 1, 2015'.
    2. The tags which contain the links 'Previous Day' and 'Next Day'.
    3. The tag which contains the number associated with the Actual Max
        Temperature.

    Returns:
        (list) A list of bs4.element.Tag objects (NOT text).
    """
    
    soup = None
    with open(filename, 'r') as file:
        soup = BeautifulSoup(file)
        
    # part 1
    tag_1 = soup.find_all(string='Thursday, January 1, 2015')[0].parent
    
    # part 2
    tags_2 = soup.find_all(string=re.compile(r'(Previous Day|Next Day)'))
    tags_2 = [ tag.parent for tag in tags_2 ]
    
    # part 3
    tag_3 = soup.find(text=re.compile(r'Max Temperature'))
    tag_3 = tag_3.parent.parent.parent.find_all('td')[1].find('span', {'class': 'wx-value'})
    
    return tag_1, tags_2, tag_3


# Problem 6
def prob6(filename="large_banks_index.html"):
    """Read the specified file and load it into BeautifulSoup. Return a list
    of the tags containing the links to bank data from September 30, 2003 to
    December 31, 2014, where the dates are in reverse chronological order.

    Returns:
        (list): A list of bs4.element.Tag objects (NOT text).
    """
    
    # read file
    soup = None
    with open(filename, 'r') as file:
        soup = BeautifulSoup(file)

    # find dates
    dates = soup.find_all("a", string=re.compile(r"[A-z]+ [0-9]+, [0-9]+"))

    # only the first is out of the correct range
    dates = dates[1:]

    return dates


# Problem 7
def prob(filename="large_banks_data.html"):
    """Read the specified file and load it into BeautifulSoup. Create a single
    figure with two subplots:

    1. A sorted bar chart of the seven banks with the most domestic branches.
    2. A sorted bar chart of the seven banks with the most foreign branches.

    In the case of a tie, sort the banks alphabetically by name.
    """
    
    soup = None
    with open(filename, 'r') as file:
        soup = BeautifulSoup(file)
        
    table_element = soup.find('table', {'cellpadding': '7'})
    
    # get the subplots ready!
    fig, axes = plt.subplots(2, 1)

    # parse the html!!!
    df = pd.read_html(str(table_element))[0]
    df["Domestic Branches"] = pd.to_numeric(df["Domestic Branches"], errors="coerce")
    domestic = df.sort_values(['Domestic Branches'], ascending=False)[:7]
    domestic['Domestic Branches'] = domestic['Domestic Branches'].astype(float)
    domestic.plot.barh(y='Domestic Branches', x='Bank Name / Holding Co Name', title='Top 7 Banks By Domestic Branches', ax=axes[0])

    df = pd.read_html(str(table_element))[0]
    df["Foreign Branches"] = pd.to_numeric(df["Foreign Branches"], errors="coerce")
    domestic = df.sort_values(['Foreign Branches'], ascending=False)[:7]
    domestic['Foreign Branches'] = domestic['Foreign Branches'].astype(float)
    domestic.sort_values(['Foreign Branches'], ascending=False).plot.barh(y='Foreign Branches', x='Bank Name / Holding Co Name', title='Top 7 Banks By Foreign Branches', ax=axes[1])

    plt.subplots_adjust(left=0.25)
    fig.show()

# if __name__ == '__main__':

#     prob()