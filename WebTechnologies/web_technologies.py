# solutions.py
"""Volume 3: Web Technologies. Solutions File."""

import json
import socket
import pandas as pd
import matplotlib.pyplot as plt
from random import randrange
import sys
from copy import deepcopy


# Problem 1
def prob1(filename="nyc_traffic.json"):
    """Load the data from the specified JSON file. Look at the first few
    entries of the dataset and decide how to gather information about the
    cause(s) of each accident. Make a readable, sorted bar chart showing the
    total number of times that each of the 7 most common reasons for accidents
    are listed in the data set.
    """

    # deserialize json
    objs = []
    with open('nyc_traffic.json', 'r') as file:
        objs = json.load(file)

    # fancy counting map function
    counts = dict()
    def count(row):
        factor1 = row['contributing_factor_vehicle_1']
        factor2 = row['contributing_factor_vehicle_2']
        for f in [factor1, factor2]:
            if f not in counts:
                counts[f] = 1
            else:
                counts[f] += 1

    # read json to dataframe and sort
    pd.io.json.json_normalize(objs).apply(count, axis=1)
    counts_df = pd.DataFrame({
        'reason': list(counts.keys()),
        'count':  list(counts.values())
    }) \
    .groupby('reason') \
    .sum() \
    .sort_values('count', ascending=False)

    # plot
    counts_df[:7].plot.barh(title='Accident Reasons by Cause')
    plt.show()

class TicTacToe:

    def __init__(self):
        """Initialize an empty board. The O's go first."""
        self.board = [[' ']*3 for _ in range(3)]
        self.turn, self.winner = "O", None

    def move(self, i, j):
        """Mark an O or X in the (i,j)th box and check for a winner."""
        if self.winner is not None:
            raise ValueError("the game is over!")
        elif self.board[i][j] != ' ':
            raise ValueError("space ({},{}) already taken".format(i,j))
        self.board[i][j] = self.turn

        # Determine if the game is over.
        b = self.board
        if any(sum(s == self.turn for s in r)==3 for r in b):
            self.winner = self.turn     # 3 in a row.
        elif any(sum(r[i] == self.turn for r in b)==3 for i in range(3)):
            self.winner = self.turn     # 3 in a column.
        elif b[0][0] == b[1][1] == b[2][2] == self.turn:
            self.winner = self.turn     # 3 in a diagonal.
        elif b[0][2] == b[1][1] == b[2][0] == self.turn:
            self.winner = self.turn     # 3 in a diagonal.
        else:
            self.turn = "O" if self.turn == "X" else "X"

    def empty_spaces(self):
        """Return the list of coordinates for the empty boxes."""
        return [(i,j) for i in range(3) for j in range(3)
                                        if self.board[i][j] == ' ' ]

    def make_random_move(self):
        """makes a random move"""

        # make a random empty location
        empty = self.empty_spaces()
        N = len(empty)
        rand_index = randrange(N)
        i, j = empty[rand_index]

        # make the move
        self.move(i, j)

    def is_over(self):
        """Returns whether game is over"""
        return len(self.empty_spaces()) == 0 or self.winner is not None

    def get_outcome(self):
        """Returns game outcome assuming game is over"""

        if self.winner == 'O':
            return 'WIN'
        elif self.winner == 'X':
            return 'LOSE'
        else:
            return 'DRAW'
    
    def __str__(self):
        board = deepcopy(self.board)
        for n in range(9):
            i, j = n // 3, n % 3
            board[i][j] = str(n) if board[i][j] == ' ' else board[i][j]
        return "\n---------\n".join(" | ".join(r) for r in board)

    def get_user_input(self):
        """Ask for input until it is valid"""

        # ask player for input until it is valid
        choice = None
        while choice is None:
            try:
                print('Enter a number between 0-8')
                choice = int(input())
                if choice not in range(9):
                    raise Exception()
            except Exception:
                pass

        return choice // 3, choice % 3


# Problem 2
class TicTacToeEncoder(json.JSONEncoder):
    """A custom JSON Encoder for TicTacToe objects."""

    def default(self, obj: TicTacToe) -> dict:
        if type(obj) != TicTacToe:
            raise TypeError('obj must be instance of TicTacToe!')

        # serialize
        return json.dumps({
            'dtype': 'TicTacToe', 
            'data': obj.__dict__
        })
        
        # serialize
        return {
            'dtype': 'TicTacToe', 
            'board': obj.board,
            'turn': obj.turn
        }


# Problem 2
def tic_tac_toe_decoder(obj: dict):
    """A custom JSON decoder for TicTacToe objects."""
    
    if 'dtype' in obj:
        if obj['dtype'] != 'TicTacToe':
            raise ValueError('dtype not TicTacToe!')

        # deserialize the dict and create a TicTacToe instance
        tic = TicTacToe()
        tic.__dict__.update(obj['data'])
        return tic

    raise ValueError('does not contain dtype field!')

def mirror_server(server_address=("0.0.0.0", 33333)):
    """A server for reflecting strings back to clients in reverse order."""
    print("Starting mirror server on {}".format(server_address))

    # Specify the socket type, which determines how clients will connect.
    server_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_sock.bind(server_address)    # Assign this socket to an address.
    server_sock.listen(1)               # Start listening for clients.

    while True:
        # Wait for a client to connect to the server.
        print("\nWaiting for a connection...")
        connection, client_address = server_sock.accept()

        try:
            # Receive data from the client.
            print("Connection accepted from {}.".format(client_address))
            in_data = connection.recv(1024).decode()    # Receive data.
            print("Received '{}' from client".format(in_data))

            # Process the received data and send something back to the client.
            out_data = in_data[::-1]
            print("Sending '{}' back to the client".format(out_data))
            connection.sendall(out_data.encode())       # Send data.

        # Make sure the connection is closed securely.
        finally:
            connection.close()
            print("Closing connection from {}".format(client_address))

def mirror_client(server_address=("0.0.0.0", 33333)):
    """A client program for mirror_server()."""
    print("Attempting to connect to server at {}...".format(server_address))

    # Set up the socket to be the same type as the server.
    client_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client_sock.connect(server_address)    # Attempt to connect to the server.
    print("Connected!")

    # Send some data from the client user to the server.
    out_data = input("Type a message to send: ")
    client_sock.sendall(out_data.encode())              # Send data.

    # Wait to receive a response back from the server.
    in_data = client_sock.recv(1024).decode()           # Receive data.
    print("Received '{}' from the server".format(in_data))

    # Close the client socket.
    client_sock.close()

# Problem 3
def tic_tac_toe_server(server_address=("0.0.0.0", 44444)):
    """A server for playing tic-tac-toe with random moves."""
    
    print("Starting mirror server on {}".format(server_address))

    # Specify the socket type, which determines how clients will connect.
    server_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_sock.bind(server_address)    # Assign this socket to an address.
    server_sock.listen(1)               # Start listening for clients.

    # Wait for a client to connect to the server.
    print("\nWaiting for a connection...")
    connection, client_address = server_sock.accept()

    done = False
    while not done:

        try:
            # Receive data from the client.
            print("Connection accepted from {}.".format(client_address))
            in_data = connection.recv(1024).decode()    # Receive data.
            # print("Received '{}' from client".format(in_data))

            if in_data == '':
                break

            # deserialize the game
            game_obj = json.loads(json.loads(in_data))
            game: TicTacToe = tic_tac_toe_decoder(game_obj)
            print('deserialized game:')
            print(str(game))

            # if game is over
            response = ''
            done = game.is_over()
            if done:
                response = game.get_outcome()
            else:
                game.make_random_move()
                response = json.dumps(game, cls=TicTacToeEncoder)

            # Process the received data and send something back to the client.
            print("Sending '{}' back to the client".format(response))
            connection.sendall(response.encode())       # Send data.
        except:
            # Make sure the connection is closed securely.
            server_sock.shutdown(socket.SHUT_RDWR)
            connection.close()
            print("Closing connection from {}".format(client_address))
            break

    # Make sure the connection is closed securely.
    try:
        server_sock.shutdown(socket.SHUT_RDWR)
        connection.close()
        print("Closing connection from {}".format(client_address))
    except:
        return


# Problem 4
def tic_tac_toe_client(server_address=("0.0.0.0", 44444)):
    """A client program for tic_tac_toe_server()."""

    print("Attempting to connect to server at {}...".format(server_address))

    # Set up the socket to be the same type as the server.
    client_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client_sock.connect(server_address)    # Attempt to connect to the server.
    print("Connected!")

    # create game
    game = TicTacToe()

    while not game.is_over():            

        # get user choice
        while True:
            try:
                i, j = game.get_user_input()

                # make the move
                game.move(i, j)
                break
            except Exception:
                print('Choose a different move!')

        # serialize game
        game_str = json.dumps(game, cls=TicTacToeEncoder)
        client_sock.sendall(game_str.encode())              # Send data.

        # deserialize game
        game_str  = client_sock.recv(1024).decode()           # Receive data.
        
        try:
            game_dict = json.loads(json.loads(game_str))
            game = tic_tac_toe_decoder(game_dict)
        except:
            print(game_str)

        # render
        print(str(game))

        # end the game
        if game.is_over():
            print(game.get_outcome() + '!')

    # Close the client socket.
    client_sock.close()

if __name__ == "__main__":

    if 'client' in sys.argv or 'server' in sys.argv:
        if sys.argv[1] == 'client':
            tic_tac_toe_client()
        else:
            tic_tac_toe_server()