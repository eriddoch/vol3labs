#!/usr/bin/env python
# coding: utf-8

# # Pandas 4: DatetimeIndex
# 
# ## Eric Riddoch
# 
# ## Class
# 
# ## Date

# In[1]:


import numpy as np
import pandas as pd
from matplotlib import pyplot as plt


# # Problem 1

# In[2]:


def prob1():
    """
    Create DatetimeIndex for stock market data

    Returns:
        df (DataFrame): updated DataFrame of stock market data
    """
    
    # read the datums!
    df = pd.read_csv('DJIA.csv', index_col=0)
    
    def floatable(flt, axis=1):
        try:
            float(flt)
            return True
        except:
            return False
    
    # convert date row to datetime objects
    dates = pd.to_datetime(df.index)
    df = df.set_index(dates, drop=True)
    floatables = df.VALUE.apply(floatable)
    
    df = df[floatables]
    df.VALUE = df.VALUE.apply(lambda x: float(x))
    
    return df

# prob1()


# # Problem 2

# In[3]:


def prob2():
    """
    Create data_range for index of paycheck data

    Returns:
        df (DataFrame): DataFrame of paycheck data
    """
    
    # read the datums!
    df = pd.read_csv('paychecks.csv', index_col=0, header=None)
    
    # get first and third Friday of every month
    n = len(df)
    mondays_week_1 = pd.date_range(start='3/13/2008', freq='WOM-1FRI', periods=n)
    mondays_week_3 = pd.date_range(start='3/13/2008', freq='WOM-3FRI', periods=n)
    date_index = mondays_week_1.union(mondays_week_3)[:len(df)] # only take as many as needed

    # use this date range as the index
    df = df.reset_index()
    df = df.set_index(date_index, drop=False)
        
    return df

# prob2()


# # Problem 3

# In[10]:


def prob3():
    """
    Create period range as index

    Returns:
        df (DataFrame): DataFrame of finance data
    """
    
    # read the datums!
    df = pd.read_csv('finances.csv')
    
    # create a time index by last day of each quarter
    quarters = pd.period_range(start='9/01/1978', freq="Q-OCT", periods=len(df))         .to_timestamp(how='end') # take the end of each period as a time stamp

    # apply the index
    df = df.set_index(quarters)
    
    return df

# prob3()


# # Problem 4

# In[8]:


def prob4():
    """
    Get average number of users entering a site each
    minute and each hour

    Returns:
        minutes (df): DataFrame containing the number
                      of users entering a website each minutes
        hours (df): DataFrame containing the number of users
                    entering a website each hour
    """
    
    # read the datums!
    df = pd.read_csv('website_traffic.csv')
    
    # compute duration of each visit in seconds
    df.ENTER = pd.to_datetime(df.ENTER)
    df.LEAVE = pd.to_datetime(df.LEAVE)
    df['DURATION'] = (df.LEAVE - df.ENTER).apply(lambda dt: dt.total_seconds())
    
    # group visits to start time (minute and hour)
    df = df.set_index('ENTER')
    minutes = df.resample('T').agg("mean")
    hours = df.resample('H').agg("mean")
    
    # What are we supposed to return? How will you know we computed the duration
    # in seconds? Should we also change the column names of the datafreames we're returning?
    return minutes, hours

# minutes, hours = prob4()

# minutes
    


# # Problem 5

# In[6]:


def prob5():
    """
    Find days with maximum and minimum change in stock price
    
    Returns:
        max_day (<M8[ns]): index of maximum change
        min_day (<M8[ns]): index of minimum change
    """
    
    # get the datums!
    df = prob1()
    
    # get differences from day to next day
    diff = (df - df.shift(1))
    
    # get the day with the largest gain
    max_gain_dt = diff.idxmax()
    
    diff = (df.shift(1) - df)
    
    max_loss_dt = diff.idxmax()
    
    return max_gain_dt, max_loss_dt





# # Problem 6

# In[7]:


def prob6():
    """
    Calculate and plot the rolling and exponential averages
    for window and span size 30, 120, and 365

    Returns:
        mins_roll (list): list of minimum values of rolling average
                          for window size 30, 120, and 365
        mins_exp (list): list of minimum values of exponential average
                         for span size 30, 120, and 365
    """
    
    # get datums!
    df = prob1()
    
    # plot moving averages with different window sizes
    for window_size in [30, 120, 365]:
        ax = df.plot(lw=.5)
        ax = df.ewm(span=window_size).mean().plot(color='g', lw=1, ax=ax)
        df.rolling(window=window_size).mean().plot(color='r', lw=1, ax=ax)
        ax.legend(["Actual", "EWMA", "Rolling"], loc="lower right")
        ax.set_title(f'Window and Span of {window_size}')
        plt.show()
    


# In[ ]:




